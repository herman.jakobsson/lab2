package INF101.lab2.pokemon;




public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;

    


    

    public static void main(String[] args) {
            // Remove the "Pokemon" type from the following lines
            pokemon1 = new Pokemon("Pikachu", 94, 12);
            pokemon2 = new Pokemon("Oddish", 100, 3);
        

        // Print initial stats
        System.out.println(pokemon1.getName() + " HP: (" + pokemon1.getCurrentHP() + "/" + pokemon1.getMaxHP() + ") STR: " + pokemon1.getStrength());
        System.out.println(pokemon2.getName() + " HP: (" + pokemon2.getCurrentHP() + "/" + pokemon2.getMaxHP() + ") STR: " + pokemon2.getStrength());

        // Simulate the battle
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
            if (!pokemon2.isAlive()) {
                System.out.println(pokemon2.getName() + " is defeated by " + pokemon1.getName() + ".");
                break;
            }

            pokemon2.attack(pokemon1);
            if (!pokemon1.isAlive()) {
                System.out.println(pokemon1.getName() + " is defeated by " + pokemon2.getName() + ".");
                break;
            }
        }
    
    }
}
